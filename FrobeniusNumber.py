# Este fichero contiene una función para calcular el número de Frobenius de un semigrupo numérico.
# Input: una lista con los generadores coprimos de un semigrupo numérico ordenados de mayor a menor.
# Output: número de Frobenius del semigrupo numérico.

def FrobeniusNumberFast(lgen):
    #lGen = lgen # Deleted
    lGen =[lgen[i] for i in range(0, len(lgen))] # Added
    h=t=a=lgen[0]
    #lGen.pop(0) # Quitamos el primer elemento # Deleted
    b=lGen
    Q = [0 for i in range(a)]
    S = [a*lGen[-1] for i in range(a*lGen[-1])]
    S[a-1]=0
    P = [i for i in range(a)]
    P[a-1] = len(lGen)
    while(h != 0):
        QHaux = Q[h-1]
        v = h
        Q[h-1] = 0
        if(h == t):
            h = 0
        else:
            h = QHaux
        for j in range(1,P[v-1]+1):
            e = (b[j-1]+v) % a
            w = b[j-1] + S[v-1]
            if(w < S[e-1] and e != 0 ):
                S[e-1] = w
                P[e-1] = j
                if(Q[e-1] == 0):
                    if(h == 0 ):
                        h = e
                        Q[e-1] = e
                        t = e
                    else:
                        Q[t-1] = e
                        Q[e-1] = e
                        t = e
    Saux = [S[i] for i in range(0,a)]
    return(max(Saux)-a)
    
    # Fuente:
    # http://www.emis.de/journals/EJC/Volume_12/PDF/v12i1r27.pdf