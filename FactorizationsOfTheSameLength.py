# Este archivo contiene varias funciones relacionadas con las longitudes de las factorizaciones.
# CondicionOmega:
#    Input: Cojunto de factorizaciones.
#    Output: True si existen dos de la misma longitud, False en caso contrario.
# CalcularW:
#    Input: Lista con los generadores de un semigrupo numérico.
#    Output: Menor elemento con al menos dos factorizaciones de la misma longitud.
# Exacto2Factorizaciones:
#    Input: Lista con los generadores de un semigrupo numérico.
#    Output: Menor elemento a partir del cual todos los elemetos del semigrupo tienen al menos dos factorizaciones de la misma longitud.
# Buscar2Factorizaciones:
#    Input: Lista con los generadores de un semigrupo numérico y un elemento del semigrupo.
#    Output: Dos factorizaciones con la misma longitud o -1 en caso de que no existan.


# Charge libraries

import numpy as np
import ImportFromBitbucket
ImportFromBitbucket.loadPyFile('NumericalSemigroups.py')
from NumericalSemigroups import *
import NumericalSemigroups
ImportFromBitbucket.loadPyFile('FrobeniusNumber.py')
from FrobeniusNumber import*
import FrobeniusNumber

# Compute w

# Esta función compruba si existen dos soluciones con la misma longitud
def CondicionOmega(soluciones):
    conjuntoLongitudes = []
    if(soluciones != []):
        for i in range(0,len(soluciones)):
            conjuntoLongitudes.append(np.sum(soluciones[i]))
        conjLongAux = list(set(conjuntoLongitudes))
        if(len(conjLongAux) != len(conjuntoLongitudes)):
            return(True)
    return(False)  
    
# Esta función calcula el menor elemento del semigrupo con dos factorizaciones con la misma longitud
def CalcularW(lgen):
    segundoGenerador = lgen[1]
    n = segundoGenerador
    condicionW = False
    while(condicionW == False):
        soluciones = FrobeniusSolve(lgen,n,False)
        solucionesLista = [list(soluciones[i]) for i in range(0,len(soluciones))]
        condicionW = CondicionOmega(solucionesLista)
        n = n+1
    return(n-1)
    
# Esta función da el valor a partir del cual todos los elementos tienen dos factorizaciones al menos con la misma longitud
def Exacto2Factorizaciones(lgen):
    n = FrobeniusNumberFast(lgen)+CalcularW(lgen)+1
    nAux = n-1
    while(True):
        soluciones = FrobeniusSolve(lgen,nAux,False)
        sol = [list(soluciones[i]) for i in range(0,len(soluciones))]
        if(sol != []):
            if(CondicionOmega(sol)):
                n = nAux
                nAux = nAux -1
            else:
                return(n)
        else:
            nAux = nAux - 1
        if(nAux <= 0):
            return(n)
            
# Esta función busca dos factorizaciones con la misma longitud.
# Importante: busca 2, no todas.
def Buscar2Factorizaciones(lgen,x):
    soluciones = FrobeniusSolve(lgen,x,False)
    if(soluciones != []):
        sol = [list(soluciones[i]) for i in range(0,len(soluciones))]
        conjuntoLongitudes = []
        conjuntoLongitudes.append(np.sum(sol[0]))
        for i in range(1,len(sol)):
            for j in range(0,i):
                if(conjuntoLongitudes[j] == np.sum(sol[i])):
                    return([sol[j],sol[i]])
                else:
                    conjuntoLongitudes.append(np.sum(sol[i]))
        return(-1)
    else:
        return(-1)