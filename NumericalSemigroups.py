
# coding: utf-8

# In[23]:

'''
NumericalSemigroups is a module with functions and classes to work with numerical semigroups. 
It contains:
* the class NumSemigroup, 
* the functions makeNSTree and showTree that show trees of numerical semigroups,
* the function IterativeInorder,
* the function showListOfSemigroups,
* the function reproduceStrPos,
* the function listStronglyPositive.
To get examples just type:
* help(NumSemigroup),
* help(makeNSTree), help(showTree),
* help(IterativeInorder),
* help(showListOfSemigroups),
* help(reproduceStrPos)
* help(listStronglyPositive)
and write the code after the symbols \'>>>\'
'''
pass


# ---
# 
# <span style="color: #000000; font-family: Lucida Console; font-size: 2em;">
# <b>Numerical semigroups</b>
# </span>
# 
# ---

# In[14]:

print(__doc__)


# # Imported libraries and `gcdL` function

# In[1]:

import sympy
from sympy import symbols, Poly, poly, pprint, init_session
init_session(quiet=True)

import numpy as np, sys, time
from numpy import array, dtype
from matplotlib import pyplot as plt
plt.style.use('ggplot')
if sys.version_info[1]==5:
    from math import gcd
else:
    from fractions import gcd

def gcdL(l):
    if len(l)==1:
        return l[0]
    if len(l)==2:
        return gcd(l[0],l[1])
    else:
        aux=l[2:]+[gcd(l[0],l[1])]
        return gcdL(aux)

from ete3 import Tree, TreeStyle


# # Functions for the class NumSemigroup: `FrobeniusSolve`, `smgS`, `quitaElementoMinimal`, `Hijos`

# In[2]:

def FrobeniusSolve(lgen,x,onlyFirst=True):
    '''
    FrobeniusSolve([55,71,99],1000,onlyFirst=False)
    '''
    posAModificar=0
    sumando=True
    dim = len(lgen)
    ceros=np.array([0 for i in range(dim)],dtype=np.int)
    xaux=x
    tuplaActual=array([0 for i in range(dim)],dtype=np.int)
    #print(xaux,tuplaActual)    
    if x==0:
        return tuplaActual
    soluciones=[]
    while( not( np.all(tuplaActual==ceros) and not(sumando) ) ):
        #print(tuplaActual)
        if sumando:
            if xaux>=lgen[0]:
                tuplaActual[posAModificar]=tuplaActual[posAModificar]+1
                xaux=xaux-lgen[posAModificar]
                if xaux==0:
                    if onlyFirst:
                        return tuplaActual
                    else:
                        #print(tuplaActual)
                        soluciones.append(array(tuplaActual))
                        sumando=False
                        continue
            if xaux!=0 and xaux<lgen[0]:
                sumando=False
                continue
        else:
            if ( posAModificar == dim-1 ) and ( tuplaActual[posAModificar] > 0 ):
                xaux=tuplaActual[posAModificar]*lgen[posAModificar]+xaux
                tuplaActual[posAModificar]=0
                posAModificar=posAModificar-1
                continue
            if ( posAModificar < dim-1 ) and ( tuplaActual[posAModificar] > 0 ):
                xaux=xaux+lgen[posAModificar]
                tuplaActual[posAModificar]=tuplaActual[posAModificar]-1
                posAModificar=posAModificar+1
                sumando=True
                continue
            if ( posAModificar < dim-1 ) and ( tuplaActual[posAModificar] == 0 ):
                posAModificar=np.max( [i for i in range(dim) if tuplaActual[i]!=0] )
                xaux=lgen[posAModificar]+xaux
                tuplaActual[posAModificar]=tuplaActual[posAModificar]-1
                sumando=True
                posAModificar=posAModificar+1
                continue
    #return array([],dtype=np.int)
    return soluciones


# In[17]:

# np.unique(array([np.sum(x) for x in FrobeniusSolve([55,71,99],2000,onlyFirst=False)]))


# In[18]:

# FrobeniusSolve([53,71,105],1357,onlyFirst=False)


# In[3]:

def smgS(sgS1):
    '''
    smgS(array([5,14,7,14],dtype=np.int))
    '''
    smgS=array([],dtype=np.int)
    sgS1.sort()
    #print(sgS1)
    sgS=np.unique(sgS1)
    #print(sgS)
    if len(sgS)==1:
        return sgS
    smgS=np.append(smgS,sgS[0])
    #print(sgS,smgS)
    for i in range(1,len(sgS)):
        if sgS[i] % smgS[0] != 0:
            smgS=np.append(smgS,sgS[i])
            break
    #print(i,sgS,smgS)
    for j in range(i+1,len(sgS)):
        if len(FrobeniusSolve(smgS,sgS[j]))==0:
            smgS=np.append(smgS,sgS[j])
    return smgS


# In[4]:

def quitaElementoMinimal(smg,x):
    '''
    quitaElementoMinimal(array([7,11,13]),11)
    '''
    if np.all(smg==array([1],dtype=np.int)) and x==1:
        return array([2,3],dtype=np.int)
    saux=array(smg)
    pos=np.where(saux==x)[0][0]
    saux=np.delete(saux,pos)
    #saux.sort()
    m=saux[0]
    laux=array( [ x+saux[i] for i in range(0,len(saux)) ] ,dtype=np.int)
    saux=np.append(saux, np.append( array([2*x,3*x],dtype=np.int), laux ) )
    #print("saux:",saux)
    return smgS(saux)


# In[5]:

def Hijos(padre): # calculo de los hijos
    '''
    Hijos( (array([1]),-1) )
    Hijos( (array([2,5]),3) )
    '''
    sg,nf=padre
    familia=[]
    n=len(sg)
    for i in range(n):
        if nf < sg[i]:
            sistGen=quitaElementoMinimal(sg,sg[i])
            familia=familia+[(sistGen,sg[i])]
    return familia


# # Functions for the class NumSemigroup: getPresentation

# In[6]:

def getPresentationOfNS(ns):
    msg=ns.getMSG()
    p=len(msg)
    #print( ' '.join(['x'+str(i) for i in range(1,p+1)])+' x' )
    lx=sympy.symbols('x ' +' '.join(['x'+str(i) for i in range(1,p+1)]))
    lpols=[ lx[0]**int(msg[i])-lx[i+1] for i in range(p) ]
    #print(lpols)
    gb=sympy.groebner(  lpols, *lx , order='lex' )
    gb=[poly(p) for p in gb]
    gb1=[p for p in gb if not np.any( array([v==lx[0] for v in p.gens]) )]
    #np.any( array([v==lx[0] for v in gb[0].gens]) ),gb[0].gens #.canonical_variables
    #print(gb)
    #for p in gb1:
    #    print(p.as_expr())
    return gb1


# # Trees

# In[23]:

def makeNSTreeCad(ns, levels=0):
    if levels==0:
        cadNS=str(ns)
        #tns=Tree(cadNS,format=1)
    else:
        hijos=ns.children()
        if len(hijos)>0:
            cadNS='('+','.join([makeNSTreeCad(s, levels=levels-1) for s in hijos])+')'+str(ns)
        else:
            cadNS=str(ns)
        #print(cadNS)
        #tns=Tree(cadNS, format=1)
    return cadNS



def makeNSTree(ns, levels=0):
    '''
    >>> ns=NumSemigroup([1])
    >>> tns=makeNSTree(ns,5)
    >>> print(tns.get_ascii())
    '''
    return Tree(makeNSTreeCad(ns,levels)+';', format=1)

get_ipython().magic('matplotlib inline')
def showTree(tree, width=200, tree_style=TreeStyle()):
    '''
    >>> tns=makeNSTree(ns,5)
    >>> showTree(tns,250) 
    >>> circular_style = TreeStyle()
    >>> circular_style.mode = "c" # draw tree in circular mode
    >>> circular_style.scale = 20
    >>> circular_style.show_leaf_name = True
    >>> circular_style.show_scale = False
    >>> showTree(tns,  tree_style=circular_style)
    ''' 
    tree.render("showTree___.png", units='mm', tree_style=tree_style)  #, w=183, units="mm")
    img=plt.imread('./showTree___.png')
    plt.axis('off')
    plt.imshow(img )
    plt.show()


# In[10]:

#ns=NumSemigroup([5,6,7,8,9])
#tns=makeNSTree(ns,4)
#print(tns.get_ascii())

#showTree(tns,250)


# In[11]:

#ns=NumSemigroup([3,5,7])
#tns=makeNSTree(ns,5)
#print(tns.get_ascii())


# In[12]:

#ns=NumSemigroup([4,5,6,7])
#tns=makeNSTree(ns,4)
#print(tns.get_ascii())


# In[13]:

#ns1=NumSemigroup([6,8,9,19,13,11,10,7,5])
#ns1.getFN(),ns1.getMSG()


# # Strongly Positive

# In[16]:

def StronglyPositive( ns ):
        smg,nf=ns.getMSG(),ns.getFN()
        dim=len(smg)
        for i in range(1,dim-1):
            l1=list( smg[:i] )
            l2=list( smg[:i+1] )
            #print(i,l1,l2)
            v1=gcdL( l1 )
            v2=gcdL( l2 )
            if v1*smg[i] >= v2*smg[i+1]:
                return False
        return True


# In[21]:

def reproduceStrPos( ns , maxHuecos ):
    '''
    ns=NumSemigroup([1])
    print(StronglyPositive(ns))
    ls=reproduceStrPos( ns , 10 )
    for x in ls:
        if x:
            for y in x:
                print(y.getMSG(),";", y.getFN(),";",StronglyPositive(y),end=' - ')
            print()
    ls
    '''
    maxFN=2*maxHuecos-1
    listaHijos = [ [] for x in range(maxHuecos+1) ]
    baux = ns.getFN()
    if ns.getFN() == -1:
        baux = 1
    topeN = maxFN // baux
    msgNS = ns.getMSG()
    if len(msgNS)==1:
        nI = 1
    elif len(msgNS)==2:
        nI = msgNS[0]
    else:
        gcdI = gcdL(list(msgNS))
        gcdImenos1 = gcdL(list(msgNS[:-1]))
        #print("--",msgNS, msgNS[:-1], gcdI, gcdImenos1)
        nI = gcdImenos1 // gcdI
    #print( nI , topeN )
    for n in range( 2 , topeN+1 ):
        ultG = msgNS[-1] * n
        primerGamma = nI * ultG + 1
        aux = n*ns.getFN() + (n-1)*primerGamma
        while aux <= maxFN:
            if gcd(n , primerGamma) == 1:
                sistGen = np.append( n*msgNS , primerGamma )
                aux = n*ns.getFN() + (n-1)*primerGamma                
                nsAux = NumSemigroup( sistGen , aux )
                #print(ultG , primerGamma , aux, nsAux.getMSG() , nsAux.getFN() )
                if aux <= maxFN:
                    #print(aux,nsAux.getMSG(),nsAux.getFN())
                    listaHijos[(aux+1)//2].append( nsAux )
                #print(listaHijos)
            primerGamma+=1
            
    return listaHijos


# In[ ]:

def listStronglyPositive(topFN):
    '''
    ls=listaStronglyPositive(100)
    '''
    maxHuecos=(topFN+1)//2
    listaSemigrupos=[[] for x in range(maxHuecos+1)]
    listaSemigrupos[0]=[NumSemigroup([1])]
    for i in range(maxHuecos):
        for sg in listaSemigrupos[i]:
            laux=reproduceStrPos(sg , maxHuecos)
            for i in range(len(laux)):
                listaSemigrupos[i]=listaSemigrupos[i]+laux[i]
    return listaSemigrupos


# # Class NumSemigroup

# In[19]:

class NumSemigroup:
    '''
    Class for representing a numerical semigroup:
    ns=NumSemigroup([5,11,13,16])
    '''
    def __init__(self,lgen,fn=None):
        self.lgen=array(lgen,dtype=np.int)
        self.fn=fn
        self.multiplicity=np.min([x for x in lgen if x!=0])
        if fn==None:
            self.FNdone=False
        else:
            self.FNdone=True
        self.MSGdone=False
        self.lAperydone=False
        self.lApery=array([],dtype=np.int)
    def __str__(self):
        return('{ '+str(self.getMSG()).replace('[','<').replace(']','>')+' '+str(self.getFN())+' }')
    def printAttrs(self):
        print('lgen',self.lgen,'fn',self.fn,'mult',self.multiplicity,'FNdone',self.FNdone,'MSGdone',
              self.MSGdone,'lAperydone',self.lAperydone,'Apery',self.lApery)
    def belongs(self,x):
        '''
        Check if the element x belong to the numerical semigroup:
        >>> ns.belongs(17)
        '''
        if x==0:
            return True
        if 0<x and x<self.multiplicity:
            return False
        if len(self.lgen[self.lgen==x])>0:
            return True
        if self.FNdone and x>self.fn:
            return True
        self.getMSG()
        expression=FrobeniusSolve(self.msg,x)
        if len(expression)>0:
            return True
        else:
            return False
    def getExpression(self,x):
        '''
        Computes an factorization of x. 
        >>> ns.getExpression(23)
        '''
        self.getMSG()
        return FrobeniusSolve(self.msg,x)
        
    def getMSG(self):
        '''
        Computes a minimal system of generator of the numerical semigroup.
        >>> ns.getMSG()
        '''
        if not self.MSGdone:
            self.msg=smgS(self.lgen)
            self.MSGdone=True
        return self.msg
    def getApery(self):
        '''
        Computes the Apéry set of the numerical semigroup.
        >>> ns.getApery()
        '''
        if not self.lAperydone:
            if self.multiplicity==1:
                self.fn=-1
                self.lApery=array([0],dtype=np.int)
                #return self.lApery
            else:
                lApery=array([x for x in range(self.multiplicity)],dtype=np.int)
                self.fn=self.getMultiplicity()-1
                for i in range(1,self.multiplicity):
                    x=i
                    while not self.belongs(x):
                        x=x+self.multiplicity
                    lApery[i]=x
                self.lApery=array(lApery,dtype=np.int)
                self.lAperydone=True
        return self.lApery
    
    def getFN(self):
        '''
        Computes the Frobenius number of the numerical semigroup.
        >>> ns.getFN()
        '''
        if not self.FNdone:
            self.getApery()
            self.fn=np.max(self.lApery)-self.multiplicity
        return self.fn
    
    def getMultiplicity(self):
        '''
        Returns the multiplicity of the numerical semigroup.
        >>> ns.getMultiplicity()
        '''
        return self.multiplicity
    
    def removeMinimalElement(self,minimalToRemove,IsTheNewFN=False):
        '''
        Returns the numerical semigroup obtained when removing a minimal element of the numerical semigroup.
        >>> ns1=ns.removeMinimalElement(11)
        '''
        smg=self.getMSG()
        if len(smg[smg==minimalToRemove])>0:
            sgAux=quitaElementoMinimal(smg,minimalToRemove)
            #print('sgAux',sgAux)
            if IsTheNewFN:
                return NumSemigroup(sgAux,minimalToRemove)
            else:
                return NumSemigroup(sgAux)
        else:
            return None
    
    def children(self):
        '''
        Returns the list formed by the numerical semigroups obtained after removing the minimal generators greater than the Frobenius number.
        >>> ns.children()
        '''
        self.getFN()
        n=len(self.getMSG())
        listOfChildren=[]
        for i in range(n):
            if self.fn < self.msg[i]:
                listOfChildren.append(self.removeMinimalElement(self.msg[i],True))
        return listOfChildren
        
    def __eq__(self,sn1):
        '''
        Checks if two numerical semigroups are equal.
        >>> ns1=NumSemigroup([5,11,16])
        >>> ns2=NumSemigroup([5,11])
        >>> ns1==ns2
        '''
        return np.all(self.getMSG()==sn1.getMSG())
    def lessThanSemigroupOrder(self,x,y):
        '''
        Checks if there exists z in the numerical semigroup such that x+z is equal to y.
        >>> ns.lessThanSemigroupOrder(21,33)
        '''
        if self.belongs(y-x):
            return True
        return False
    def getGaps(self):
        '''
        Returns the set of positive integer not belonging to the numerical semigroup.
        >>> ns.getGaps()
        '''
        smg=self.getMSG()
        laux=self.getApery()
        m=smg[0]
        lgaps=[]
        for x in laux:
            x=x-m
            while x>0:
                lgaps.append(x)
                x=x-m
        return np.sort(lgaps)
    def isPFN(self,x):
        '''
        Checks if x is a pseudo-Frobenius number.
        >>> ns.isPFN(11)
        '''
        if self.belongs(x):
            return False
        else:
            smg=self.getMSG()
            for y in smg:
                if not self.belongs(x+y):
                    return False
            return True
    def getPFN(self):
        '''
        Returns the list of pseudo-Frobenius numbers.
        >>> ns.getPFN()
        '''
        lgaps=self.getGaps()
        lPFN=[]
        smg=self.getMSG()
        for x in lgaps:
            t=True
            for y in smg:
                if not self.belongs(x+y):
                    t=False
                    break
            if t:
                lPFN.append(x)
        return lPFN
    def isSymmetric(self):
        '''
        Checks if the numerical semigroup is symmetric.
        >>> ns.isSymmetric()
        '''
        fn=self.getFN()
        if fn % 2 == 0:
            return False
        lgaps=self.getGaps()
        nGaps=len(lgaps)
        if (fn+1)//2 == nGaps:
            return True
        return False
    def isContainedIn(self,sn):
        '''
        Checks if the numrical semigroup is contained in another numerical semigroup.
        >>> ns1=NumSemigroup([2,5])
        >>> ns.isContainedIn(ns1)
        '''
        fn=self.getFN()
        fnsn=sn.getFN()
        if fnsn==-1:
            return True
        if fn==-1 and fnsn>-1:
            return False
        smg=self.getMSG()
        for x in smg:
            if not sn.belongs(x):
                return False
        return True
    def intersection(self, sn1):
        '''
        Returns the numerical semigroup intersection of the numerical semigroup with another numerical semigroup.
        >>> ns1=NumSemigroup([4,7,9])
        >>> ns2=ns.intersection(ns1)
        >>> print(ns2)
        '''
        fn1=self.getFN()
        fn2=sn1.getFN()
        if fn1==-1:
            return sn1
        if fn2==-1:
            return self
        fn=max([fn1,fn2])
        laux=[]
        for x in range(1,fn):
            #print(laux)
            if self.belongs(x) and sn1.belongs(x):
                laux.append(x)
        #print('laux:',laux)
        m=laux[0]
        #print(m)
        laux=laux+[fn+i+1 for i in range(m)]
        #print(laux)
        sn=NumSemigroup(laux,fn)
        return sn
    def sum(self, sn1):
        '''
        Computes the sum of the numerical semigroup with another numerical semigroup.
        >>> ns1=NumSemigroup([4,7,9])
        >>> ns2=ns.sum(ns1)
        >>> print(ns2)
        '''
        sgAux=np.concatenate((self.getMSG(),sn1.getMSG()))
        sgAux=np.unique(sgAux)
        return NumSemigroup(sgAux)
    
    def Z(self,x):
        '''
        Computes the set of factorization of x
        >>> ns.Z(55)
        '''
        return array(FrobeniusSolve(self.getMSG(),x,onlyFirst=False), dtype=np.int)
    def L(self,x):
        '''
        Computes the set of length of x
        >>> ns.L(x)
        '''
        return np.unique(array([np.sum(x) for x in FrobeniusSolve(self.getMSG(),x,onlyFirst=False)]))
    def getPresentation(self):
        '''
        Computes a presentation of the numerical semigroup.
        >>> ns.getPresentation()
        '''
        return getPresentationOfNS(self)
    def isStronglyPositive( self ):
        '''
        Checks if a numerical semigroup is Strongly Increasing
        '''        
        return StronglyPositive(self)


# # Examples

# In[26]:

#ns=NumSemigroup([3,5,7])
#print( ns.getFN() )
# l=ns.children()
#print( ns.Z(17) )


# In[27]:

# len(l)


# In[28]:

# print( NumSemigroup([3,5]).sum(NumSemigroup([3,7])) )
#NumSemigroup([15,19,27]).L(200)


# s=NumSemigroup([121,165,205])
# x=7500
# l=s.Z(x)
# print(l)
# [np.sum(x) for x in l]


# In[29]:

# s=NumSemigroup([55,71,99])
# x=2000
# l=s.Z(x)
# print(l)
# [np.sum(x) for x in l]


# In[30]:

# s.getFN()


# In[31]:

#s1=NumSemigroup([2,5])
#s2=NumSemigroup([2,3])
#s2.intersection(s1)


# In[32]:

# ns=NumSemigroup([4,6,13])
# print(ns.getGaps(), ns.isSymmetric())
# ns.getPFN()


# In[33]:

# sn1=NumSemigroup([2,3])
# sn2=NumSemigroup([1])
# sn=sn2.intersection(sn1)
# print(sn)


# In[14]:

#s1=NumSemigroup([2,3,4])
#s1.getPresentation()
# s2=NumSemigroup([2,7])
# s1!=s2


# # Functions: `IterativeInorder`, `showListOfSemigroups`

# In[8]:

def IterativeInorder(nivel,fProp,todos):
    '''
    This functions computes the numerical semigroups verifying the function fProp with least than or equal number of gaps to nivel.
    >>> lls=IterativeInorder(10,lambda x: True,True)
    >>> print([len(x) for x in lls])
    >>> showListOfSemigroups(lls)
    '''
    sgActual=NumSemigroup([1],-1)
    fase=0
    listaEncima=[]
    nfAnterior=-2
    listaFProp=[]
    contador=[0 for i in range(nivel+1)]
    lfProp=[[] for i in range(nivel+1)]
    #print(len(lfProp))
    while( not(  sgActual.getFN()==-1 and ( nfAnterior==1 ) ) ):
        #print(sgActual, fase, nivel, lfProp)
        if nfAnterior < sgActual.getFN():
            if fase == nivel:
                if fProp(sgActual):
                    contador[fase]=contador[fase]+1
                    #if almacenar:
                    #    listaFProp.append(sgActual)
                    if todos:
                        #print('fase-1=',fase-1)
                        lfProp[fase].append(sgActual)
                nfAnterior=sgActual.getFN()
                sgActual=listaEncima[-1]
                listaEncima=listaEncima[0:-1]
                fase=fase-1
                continue
            if fase < nivel:
                if fProp(sgActual):
                    if todos:
                        lfProp[fase].append(sgActual)
                    contador[fase]=contador[fase]+1
                generadoresBuenos=[x for x in sgActual.getMSG() if x > sgActual.getFN() ]
                if len(generadoresBuenos)==0:
                    nfAnterior=sgActual.getFN()
                    sgActual=listaEncima[-1]
                    listaEncima=listaEncima[0:-1]
                    fase=fase-1
                    continue
                else:
                    nfPrimerHijo=np.min(generadoresBuenos)
                    sgPrimerHijo=sgActual.removeMinimalElement(nfPrimerHijo,True)
                    nfAnterior=sgActual.getFN()
                    listaEncima.append(sgActual)
                    sgActual=sgPrimerHijo
                    fase=fase+1
                    continue
        if sgActual.getFN() < nfAnterior:
            generadoresBuenos = [ x for x in sgActual.getMSG() if x > nfAnterior ]
            if len(generadoresBuenos) == 0:
                nfAnterior=sgActual.getFN()
                sgActual=listaEncima[-1]
                listaEncima=listaEncima[0:-1]
                fase=fase-1
                continue
            else:
                nfHijo=np.min(generadoresBuenos)
                sgHijo=sgActual.removeMinimalElement(nfHijo,True)
                nfAnterior=sgActual.getFN()
                listaEncima.append(sgActual)
                sgActual=sgHijo
                fase=fase+1
                continue
    if todos:
        return lfProp
    #if almacenar:
    #    return (contador,listaFProp)
    else:
        return contador


# In[36]:

# lls=IterativeInOrden(10,lambda x: True,True)
# print([len(x) for x in lls])


# In[9]:

def showListOfSemigroups(ls,new='* ',endline='\n'):
    '''
    Function to print the numerical semigroups of a list of numerical semigroups.
    >>> lls=IterativeInorder(10,lambda x: True,True)
    >>> print([len(x) for x in lls])
    >>> showListOfSemigroups(lls)
    '''
    for x in ls:
        if x:
            cad=''
            for y in x:
                cad=cad + str(y) + ', '
            cad=cad[:-2]
            print(new + cad, end=endline)


# In[38]:

# print( NumSemigroup([2,7]).sum(NumSemigroup([3,5,7])) )


# In[39]:

# showListOfSemigroups(lls)


# In[ ]:



