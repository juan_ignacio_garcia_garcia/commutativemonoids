
# coding: utf-8

# In[4]:

import numpy
from fractions import Fraction
from numpy import array


def xIsleastThan(x,lElements):
    '''
    x: array of shape (p,)
    lElements: array of shape (n,p).
    returns an array of indices of the rows of lElements that x is least than.
    >>> xIsleastThan(array([3,2]),array([[2,1],[5,5],[0,3]]))
    array([1], dtype=int64)
    >>> list(xIsleastThan(array([3,2]),array([[2,1],[5,5],[0,3]])))
    [1]
    '''
    li=numpy.min(lElements-x,axis=1)
    return numpy.where(li>=0)[0]
def elementsLeastThanX(x,lElements):
    '''
    x: array of shape (p,)
    lElements: array of shape (n,p).
    returns an array of indices of the rows of lElements lower that x.
    >>> elementsLeastThanX(array([3,2]),array([[2,1],[5,5],[0,3]]))
    array([0], dtype=int64)
    >>> list(elementsLeastThanX(array([3,2]),array([[2,1],[5,5],[0,3]])))
    [0]
    '''   
    li=numpy.min(x-lElements,axis=1)
    return numpy.where(li>=0)[0]

def insertInIncomp(x,lIncomp):
    '''The elements of the list lIncomp are incomparable, x is inserted 
    if x is incoparable with them or if it is lower. In such case
    greater elements are removed
    >>> insertInIncomp(array([1,2]),array([[2,2],[5,0],[1,3]]))
    array([[5, 0],
       [1, 2]])
    '''
    x=x.reshape(1,x.shape[0])
    if len(lIncomp)==0:
        return x
    aux1=numpy.where(                numpy.any(                    lIncomp-x<numpy.zeros_like(lIncomp),axis=1) )[0]
    lIncomp1=array([lIncomp[i] for i in aux1])
    shaux=(1,x.shape[0])
    if len(lIncomp)>len(lIncomp1):
        if len(lIncomp1)>0:
            lIncomp1=numpy.concatenate((lIncomp1,x),axis=0)
            return lIncomp1
        else:
            return x
    for y in lIncomp1:
        if numpy.all(y<=x):
            return lIncomp1
    lIncomp1=numpy.concatenate((lIncomp1,x),axis=0)
    return lIncomp1

def computeMinimals(lx):
    '''Compute the minimals of the list of np.array lx
    >>> computeMinimals([array([1,2,3]),array([5,3,4])])
    [array([1, 2, 3])]
    >>> computeMinimals(array([array([1,2,3]),array([5,3,3]),array([0,5,0])]))
    array([[1, 2, 3],
       [0, 5, 0]])
    '''
    lI=array([])
    for x in lx:
        #print('Vamos insertar x=',x, ' ',type(x) ,' en ',lI, ' ', type(lI))
        lI=insertInIncomp(x,lI)
    return lI

proj=numpy.vectorize(lambda x:x if x>0 else 0,otypes=[Fraction])

#proj(array([-5,2,3]))
# doctest.run_docstring_examples(insertInIncomp,globals(),verbose=True)
# doctest.run_docstring_examples(computeMinimals,globals(),verbose=True)


# In[5]:

insertInIncomp(array([1,2]),array([[2,2],[5,0],[1,3]]))


# In[6]:

proj(array([[-5,2,-9],[-1,2,3]],dtype=Fraction))


# In[ ]:



